import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import { AiOutlineTwitter, AiOutlineMenu } from "react-icons/ai";
import { SiDiscord } from "react-icons/si";

import {
    VerticalTimeline,
    VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import SSSAccordion from "../components/accordion/SSSAccordion";

export default function Home() {
    return (
        <>
            <header>
                <div className="w-full pt-16 relative min-h-screen">
                    <div
                        className="w-full z-50  absolute top-0 left-0 right-0"
                        id="section-home"
                    >
                        <label
                            htmlFor="toggle-menu"
                            className="px-8 block md:hidden py-6"
                        >
                            <AiOutlineMenu size={28} color="#fff" />
                        </label>
                        <input
                            type="checkbox"
                            name="toggle-menu"
                            id="toggle-menu"
                            className="toggle-menu hidden"
                        />
                        <div className="dropdown-menu hidden md:block">
                            <ul className=" md:flex justify-center">
                                <li className="md:mb-0">
                                    <a
                                        className="px-8 py-2 md:py-6 lg:py-6 block md:inline-block font-semibold text-white text-lg"
                                        href="#section-home"
                                    >
                                        Home
                                    </a>
                                </li>
                                <li className="md:mb-0">
                                    <a
                                        className="px-8 py-2 md:py-6 lg:py-6 block md:inline-block font-semibold text-white text-lg"
                                        href="#section-about"
                                    >
                                        About
                                    </a>
                                </li>
                                <li className="md:mb-0">
                                    <a
                                        className="px-8 py-2 md:py-6 lg:py-6 block md:inline-block font-semibold text-white text-lg"
                                        href="#section-roadmap"
                                    >
                                        Roadmap
                                    </a>
                                </li>
                                <li className="md:mb-0">
                                    <a
                                        className="px-8 py-2 md:py-6 lg:py-6 block md:inline-block font-semibold text-white text-lg"
                                        href="#section-team"
                                    >
                                        Team
                                    </a>
                                </li>
                                <li className="md:mb-0">
                                    <a
                                        className="px-8 py-2 md:py-6 lg:py-6 block md:inline-block font-semibold text-white text-lg"
                                        href="#section-faq"
                                    >
                                        FAQ
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div
                        className="absolute top-0 bottom-0 left-0 right-0"
                        style={{
                            background: "url('images/banner-bg.gif')",
                        }}
                    ></div>
                    <div className="blue-bg bg-opacity-90 absolute top-0 bottom-0 left-0 right-0"></div>
                    <div className="absolute min-h-screen left-0 right-0 flex flex-col items-center justify-center">
                        <img
                            src="/images/sss-logo-min.png"
                            alt=""
                            className="w-1/8 mx-auto mb-12"
                        />

                        <button
                            className="bg-orange-600 text-white px-4 py-2 text-2xl font-semibold rounded-lg mb-8"
                            onClick={(e) => {
                                e.preventDefault();
                                window.location.href =
                                    "https://discord.gg/Akb4afTAZz";
                            }}
                            type="button"
                        >
                            Join The Society
                        </button>

                        {/* <div className="text-4xl text-white font-normal mb-8">
                            0.079 Ξ
                        </div>

                        <div className="w-1/12 mx-auto mb-16">
                            <div className="flex justify-center">
                                <button className="bg-white px-3 py-2 text-orange-600 font-bold rounded-l-lg text-3xl">
                                    +
                                </button>
                                <input
                                    type="text"
                                    className="bg-gray-200 text-orange-600 font-bold text-center flex-1 text-xl"
                                    value="21"
                                />
                                <button className="bg-white px-3 py-2 text-orange-600 font-bold rounded-r-lg text-3xl">
                                    -
                                </button>
                            </div>
                        </div> */}
                    </div>
                </div>
            </header>

            <div
                id="section-about"
                className="bg-blue-900"
                style={{ background: "url('images/section-1-bg-min.png')" }}
            >
                <div className="container mx-auto md:flex md:items-center py-24 ">
                    <div className="md:w-1/2 px-8 lg:px-16 mb-16 md:mb-0">
                        <img src="images/section-1-min.png" alt="" />
                    </div>
                    <div className="md:w-1/2 px-8 lg:px-16 mb-16 md:mb-0">
                        <h2 className="text-orange-500 text-3xl lg:text-5xl font-bold mb-8 text-center md:text-left">
                            WELCOME TO SUPERLATIVE SECRET SOCIETY
                        </h2>
                        <p className="mb-8 text-white md:text-left">
                            Superlative Secret Society is a collection of
                            community-driven, programmatically, randomly
                            generated NFTs using Ethereum blockchain technology.
                            This idea comes from an established secret
                            society/medieval guild who give benefit and access
                            to their members.{" "}
                        </p>
                        <p className="mb-8 text-white md:text-left">
                            The SSS consists of 11,111 randomly assembled
                            abstract works of art. SSS NFTs are constructed from
                            various personality traits, we use super
                            extraterrestrial beings as a reference for the
                            artwork. We hope the artwork of SSS will have a huge
                            positive impact for the community.
                        </p>
                    </div>
                </div>
            </div>

            <div className="bg-orange-400">
                <div className="container mx-auto py-24 ">
                    <h2 className="text-blue-900 font-bold text-3xl lg:text-5xl text-center block mb-12">
                        SUPERLATIVE PREVIEW
                    </h2>

                    <div className="container max-auto">
                        <ul className="flex flex-wrap items-start px-4 md:px-0">
                            <li className="px-4 py-4 w-1/2 md:w-1/4">
                                <img
                                    src="images/section-2-thumbnail-1-min.png"
                                    alt=""
                                />
                            </li>
                            <li className="px-4 py-4 w-1/2 md:w-1/4">
                                <img
                                    src="images/section-2-thumbnail-2-min.png"
                                    alt=""
                                />
                            </li>
                            <li className="px-4 py-4 w-1/2 md:w-1/4">
                                <img
                                    src="images/section-2-thumbnail-3-min.png"
                                    alt=""
                                />
                            </li>
                            <li className="px-4 py-4 w-1/2 md:w-1/4">
                                <img
                                    src="images/section-2-thumbnail-4-min.png"
                                    alt=""
                                />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div className="bg-purple-900">
                <div className="container mx-auto py-24 ">
                    <h2 className="text-orange-400 font-semibold text-3xl lg:text-5xl text-center block mb-12">
                        WHY “COLLECT” SSS
                    </h2>

                    <div className="flex justify-center mb-8">
                        <p className="text-center w-5/6 text-white text-lg">
                            This is a community-driven project. Being a member
                            of Superlative Secret Society is much more than just
                            owning an NFT avatar with unique artwork different
                            than others in the NFT space. We believe all SSS
                            NFTs are special so all SSS NFT holders will be
                            getting:
                        </p>
                    </div>

                    <div className="flex flex-wrap justify-center mb-16">
                        <div className="w-full md:w-1/2 lg:w-1/3 text-center px-8 lg:px-16 py-8">
                            <img
                                src="images/section-3-thumbnail-1-min.png"
                                alt=""
                                className="w-1/2 mx-auto"
                            />
                            <h3 className="text-orange-400 font-semibold text-2xl mb-4">
                                Commercial Rights
                            </h3>
                            <p className="text-white">
                                All holders of the SSS NFT will be given their
                                commercial rights over the NFT artworks that
                                they currently own
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 lg:w-1/3 text-center px-8 lg:px-16 py-8">
                            <img
                                src="images/section-3-thumbnail-2-min.png"
                                alt=""
                                className="w-1/2 mx-auto"
                            />
                            <h3 className="text-orange-400 font-semibold text-2xl mb-4">
                                Exclusive Access to Chill Room
                            </h3>
                            <p className="text-white">
                                Access to Chill room (VIP Lounge) in our
                                Superlative Gallery located in Bali
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 lg:w-1/3 text-center px-8 lg:px-16 py-8">
                            <img
                                src="images/section-3-thumbnail-3-min.png"
                                alt=""
                                className="w-1/2 mx-auto"
                            />
                            <h3 className="text-orange-400 font-semibold text-2xl mb-4">
                                Signed Framed Print
                            </h3>
                            <p className="text-white">
                                A chance to be shipped a Signed framed print of
                                your SSS NFT (up to 111 will be shipped)
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 lg:w-1/3 text-center px-8 lg:px-16 py-8">
                            <img
                                src="images/section-3-thumbnail-4-min.png"
                                alt=""
                                className="w-1/2 mx-auto"
                            />
                            <h3 className="text-orange-400 font-semibold text-2xl mb-4">
                                Voting & Suggestion Rights
                            </h3>
                            <p className="text-white">
                                Access for voting and recommendations on artists
                                or projects that need Superlative Secret Society
                                Foundation support
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 lg:w-1/3 text-center px-8 lg:px-16 py-8">
                            <img
                                src="images/section-3-thumbnail-5-min.png"
                                alt=""
                                className="w-1/2 mx-auto"
                            />
                            <h3 className="text-orange-400 font-semibold text-2xl mb-4">
                                Superlative Gallery Rights
                            </h3>
                            <p className="text-white">
                                Access for voting and recommendations on
                                artists, project artworks, or even your own
                                personal artwork to be displayed at Superlative
                                Gallery located in Bali (up to 20 artworks/month
                                will be displayed in the gallery)
                            </p>
                        </div>

                        <div className="w-full md:w-1/2 lg:w-1/3 text-center px-8 lg:px-16 py-8">
                            <img
                                src="images/section-3-thumbnail-6-min.png"
                                alt=""
                                className="w-1/2 mx-auto"
                            />
                            <h3 className="text-orange-400 font-semibold text-2xl mb-4">
                                Access to Limited Merchandise
                            </h3>
                            <p className="text-white">
                                Get your own exclusive “SSS” NFT merchandise
                                such as limited edition tees, hoodies and other
                                goods
                            </p>
                        </div>
                    </div>

                    <h2 className="text-orange-400 font-semibold px-8 lg:px-16 text-2xl md:text-4xl text-center block mb-4">
                        Your Artwork Displayed Permanently
                    </h2>

                    <p className="text-center px-8 lg:px-16  text-white text-lg">
                        We will be doing a community exclusive raffle to get the
                        10 holders artworks to be displayed in the gallery
                        permanently
                    </p>
                </div>
            </div>

            <div style={{ background: "url('images/section-4-bg-min.png')" }}>
                <div className="container mx-auto md:flex md:items-center py-24 ">
                    <div className="md:w-1/2 px-8 lg:px-16 mb-16 md:mb-0">
                        <img
                            src="images/section-4-illustration-min.png"
                            alt=""
                        />
                    </div>
                    <div className="md:w-1/2 px-8 lg:px-16 mb-16 md:mb-0">
                        <h2 className="text-yellow-500 text-3xl lg:text-5xl font-bold mb-8 text-center md:text-left">
                            THE FUTURE OF SUPERLATIVE SECRET SOCIETY
                        </h2>
                        <p className="mb-8 text-white">
                            SSS Foundation will become a DAO in the future. All
                            profit from flipping other artist/project artwork
                            and 30% profit from Superlative Gallery will be
                            divided equally to all holders each month and we
                            will continue to develop our post-sale roadmap for
                            the benefit of the community.
                        </p>
                    </div>
                </div>
            </div>

            <div
                id="section-roadmap"
                className="bg-white pt-24 overflow-x-hidden"
            >
                <div className="container mx-auto flex flex-col items-center">
                    <div className="w-11/12 px-8 lg:px-16">
                        <h2 className="text-3xl lg:text-5xl font-bold mb-8 text-center md:text-left">
                            THE ROADMAP
                        </h2>
                    </div>

                    <div className="w-11/12 px-8 lg:px-16">
                        <VerticalTimeline className="vertical-timeline-1">
                            <VerticalTimelineElement
                                position="right"
                                className="vertical-timeline-element--work"
                                contentArrowStyle={{
                                    borderRight: "none",
                                }}
                                contentStyle={{
                                    background: "transparent",
                                    boxShadow: "0px 5px 0 #fe346e !important",
                                }}
                                iconStyle={{
                                    background: "#fe346e",
                                    color: "#fff",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                icon={
                                    <div style={{ position: "relative" }}>
                                        <h1 className="font-bold text-l">
                                            10%
                                        </h1>
                                    </div>
                                }
                            >
                                <div className="vertical-timeline-element-content-inner">
                                    <p>
                                        Exclusive community raffle to win ETH
                                    </p>
                                </div>
                            </VerticalTimelineElement>
                            <VerticalTimelineElement
                                position="left"
                                className="vertical-timeline-element--work"
                                contentStyle={{
                                    background: "transparent",
                                    boxShadow: "0px 5px 0 #fe346e !important",
                                }}
                                contentArrowStyle={{
                                    borderRight: "none",
                                }}
                                iconStyle={{
                                    background: "#fe346e",
                                    color: "#fff",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                icon={
                                    <div style={{ position: "relative" }}>
                                        <h1 className="font-bold text-l">
                                            20%
                                        </h1>
                                    </div>
                                }
                            >
                                <div className="vertical-timeline-element-content-inner">
                                    <p>Exclusive community raffle to win ETH</p>
                                </div>
                            </VerticalTimelineElement>
                            <VerticalTimelineElement
                                position="right"
                                className="vertical-timeline-element--work"
                                contentStyle={{
                                    background: "transparent",
                                    boxShadow: "0px 5px 0 #fe346e !important",
                                }}
                                contentArrowStyle={{
                                    borderRight: "none",
                                }}
                                iconStyle={{
                                    background: "#fe346e",
                                    color: "#fff",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                icon={
                                    <div style={{ position: "relative" }}>
                                        <h1 className="font-bold text-l">
                                            30%
                                        </h1>
                                    </div>
                                }
                            >
                                <div className="vertical-timeline-element-content-inner">
                                    <p>
                                        $30,000 will be donated to Share The
                                        Meal (a crowdfunding platform to fight
                                        global hunger through the United Nations
                                        World Food Programme)
                                    </p>
                                </div>
                            </VerticalTimelineElement>
                            <VerticalTimelineElement
                                position="left"
                                className="vertical-timeline-element--work"
                                contentStyle={{
                                    background: "transparent",
                                    boxShadow: "0px 5px 0 #fe346e !important",
                                }}
                                contentArrowStyle={{
                                    borderRight: "none",
                                }}
                                iconStyle={{
                                    background: "#fe346e",
                                    color: "#fff",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                icon={
                                    <div style={{ position: "relative" }}>
                                        <h1 className="font-bold text-l">
                                            40%
                                        </h1>
                                    </div>
                                }
                            >
                                <div className="vertical-timeline-element-content-inner">
                                    <p>
                                        Superlative Secret Society Foundation
                                        unlocked. Together, we’ll collecting
                                        some dope artwork! 5 ETH to be allocated
                                        to the SSS Foundation (Community Wallet)
                                    </p>
                                </div>
                            </VerticalTimelineElement>
                            <VerticalTimelineElement
                                position="right"
                                className="vertical-timeline-element--work"
                                contentStyle={{
                                    background: "transparent",
                                    boxShadow: "0px 5px 0 #fe346e !important",
                                }}
                                contentArrowStyle={{
                                    borderRight: "none",
                                }}
                                iconStyle={{
                                    background: "#fe346e",
                                    color: "#fff",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                icon={
                                    <div style={{ position: "relative" }}>
                                        <h1 className="font-bold text-l">
                                            50%
                                        </h1>
                                    </div>
                                }
                            >
                                <div className="vertical-timeline-element-content-inner">
                                    <p>
                                        SSS will acquire land in the Somnium
                                        Space for SSS Foundation Metaverse
                                        Gallery
                                    </p>
                                </div>
                            </VerticalTimelineElement>
                        </VerticalTimeline>
                    </div>
                </div>
            </div>

            <div
                className="bg-pink pb-24 overflow-x-hidden"
                style={{ background: 'url("images/section-5-bg-min.png")' }}
            >
                <div className="container mx-auto flex flex-col items-center">
                    <div className="w-11/12 px-8 lg:px-16">
                        <VerticalTimeline className="vertical-timeline-2">
                            <VerticalTimelineElement
                                className="vertical-timeline-element--work"
                                contentArrowStyle={{
                                    borderRight: "none",
                                }}
                                contentStyle={{
                                    background: "transparent",
                                    color: "white",
                                    boxShadow: "0px 5px 0 #fff !important",
                                }}
                                iconStyle={{
                                    background: "#fff",
                                    color: "#fe346e",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                icon={
                                    <div style={{ position: "relative" }}>
                                        <h1 className="font-bold text-l">
                                            60%
                                        </h1>
                                    </div>
                                }
                            >
                                <div className="vertical-timeline-element-content-inner">
                                    <p>
                                        15 ETH to be allocated to SSS Foundation
                                    </p>
                                </div>
                            </VerticalTimelineElement>
                            <VerticalTimelineElement
                                className="vertical-timeline-element--work"
                                contentArrowStyle={{
                                    borderRight: "none",
                                }}
                                contentStyle={{
                                    background: "transparent",
                                    color: "white",
                                    boxShadow: "0px 5px 0 #fff !important",
                                }}
                                iconStyle={{
                                    background: "#fff",
                                    color: "#fe346e",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                icon={
                                    <div style={{ position: "relative" }}>
                                        <h1 className="font-bold text-l">
                                            70%
                                        </h1>
                                    </div>
                                }
                            >
                                <div className="vertical-timeline-element-content-inner">
                                    <p>
                                        Physical signed art for 111 SSS holders
                                        will be shipped
                                    </p>
                                </div>
                            </VerticalTimelineElement>
                            <VerticalTimelineElement
                                className="vertical-timeline-element--work"
                                contentArrowStyle={{
                                    borderRight: "none",
                                }}
                                contentStyle={{
                                    background: "transparent",
                                    color: "white",
                                    boxShadow: "0px 5px 0 #fff !important",
                                }}
                                iconStyle={{
                                    background: "#fff",
                                    color: "#fe346e",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                icon={
                                    <div style={{ position: "relative" }}>
                                        <h1 className="font-bold text-l">
                                            80%
                                        </h1>
                                    </div>
                                }
                            >
                                <div className="vertical-timeline-element-content-inner">
                                    <p>Exclusive SSS Merch Store</p>
                                </div>
                            </VerticalTimelineElement>
                            <VerticalTimelineElement
                                className="vertical-timeline-element--work"
                                contentArrowStyle={{
                                    borderRight: "none",
                                }}
                                contentStyle={{
                                    background: "transparent",
                                    color: "white",
                                    boxShadow: "0px 5px 0 #fff !important",
                                }}
                                iconStyle={{
                                    background: "#fff",
                                    color: "#fe346e",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                icon={
                                    <div style={{ position: "relative" }}>
                                        <h1 className="font-bold text-l">
                                            90%
                                        </h1>
                                    </div>
                                }
                            >
                                <div className="vertical-timeline-element-content-inner">
                                    <p>
                                        20 ETH to be allocated to SSS Foundation
                                    </p>
                                </div>
                            </VerticalTimelineElement>
                            <VerticalTimelineElement
                                className="vertical-timeline-element--work"
                                contentArrowStyle={{
                                    borderRight: "none",
                                }}
                                contentStyle={{
                                    background: "transparent",
                                    color: "white",
                                    boxShadow: "0px 5px 0 #fff !important",
                                }}
                                iconStyle={{
                                    background: "rgba(30, 58, 138, 1)",
                                    color: "#fb923c",
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                    boxShadow:
                                        "0 0 0 4px #1e3a8a, inset 0 2px 0 rgb(0 0 0 / 8%), 0 3px 0 4px rgb(0 0 0 / 5%)",
                                }}
                                icon={
                                    <div style={{ position: "relative" }}>
                                        <h1 className="font-bold text-l">
                                            100%
                                        </h1>
                                    </div>
                                }
                                dateClassName="py-0"
                                date={
                                    <h4 className="text-3xl lg:text-5xl font-bold mb-8 text-center md:text-left text-blue-900">
                                        OUR GOALS
                                    </h4>
                                }
                            >
                                <div className="vertical-timeline-element-content-inner">
                                    <p>
                                        SSS will acquire property for the
                                        gallery in the physical world located in
                                        Bali. Exclusive members of SSS will have
                                        private access to hangout in the
                                        physical world with other members. We
                                        will be doing a community exclusive
                                        raffle to get the 10 holders artwork
                                        displayed in the gallery permanently.
                                    </p>

                                    <p>
                                        Once a month the holders will be able to
                                        vote to show their NFT art or other NFT
                                        art outside the exclusive community to
                                        be displayed in the SSS Gallery.
                                    </p>
                                </div>
                            </VerticalTimelineElement>
                        </VerticalTimeline>
                    </div>
                </div>
            </div>

            <div style={{ background: "url('images/section-6-bg-min.png')" }}>
                <div className="container mx-auto md:flex flex-col-reverse md:flex-row py-24 items-center">
                    <div className="md:w-1/2 px-8 lg:px-16 mb-16 md:mb-0">
                        <h2 className="text-yellow-100 text-3xl lg:text-5xl font-bold mb-8 text-center md:text-left">
                            SUPERLATIVE SPECS
                        </h2>

                        <p className="mb-8 text-white">
                            Each SSS NFT is made up of 8 traits: sparkling
                            galaxy, background, foreground, head, body, muffler,
                            necklace, and orb. There are over 200 traits of
                            varying rarity.
                        </p>

                        <p className="mb-8 text-white">
                            Each SSS NFT is a verifiably unique ERC-721 token.
                            There will never be more than 11,111.
                        </p>

                        <p className="mb-8 text-white">
                            Ownership and commercial usage rights are granted in
                            full to the holder over their SSS NFT.
                        </p>

                        <br></br>

                        <button
                            className="bg-orange-600 text-white px-4 py-2 text-2xl font-semibold rounded-lg mb-8"
                            onClick={(e) => {
                                e.preventDefault();
                                window.location.href =
                                    "https://discord.gg/Akb4afTAZz";
                            }}
                            type="button"
                        >
                            Join The Society
                        </button>
                    </div>

                    <div className="md:w-1/2 px-8 lg:px-16 mb-16 md:mb-0">
                        <img
                            src="images/section-6-illustration-min.png"
                            alt=""
                        />
                    </div>
                </div>
            </div>

            <div id="section-team" className="bg-gray-200">
                <div className="container mx-auto md:flex py-24 items-center">
                    <div className="md:w-1/2 mb-16 md:mb-0 px-8 lg:px-16">
                        <div className="flex flex-wrap">
                            <img
                                className="w-1/2"
                                src="images/team-1-min.png"
                                alt=""
                            />
                            <img
                                className="w-1/2"
                                src="images/team-2-min.png"
                                alt=""
                            />
                            <img
                                className="w-1/2"
                                src="images/team-3-min.png"
                                alt=""
                            />
                            <img
                                className="w-1/2"
                                src="images/team-4-min.png"
                                alt=""
                            />
                        </div>
                    </div>
                    <div className="md:w-1/2 mb-16 md:mb-0 px-8 lg:px-16">
                        <h2 className="text-blue-900 text-3xl lg:text-5xl font-bold mb-8">
                            SUPERLATIVE TEAM
                        </h2>
                        <p className="mb-8">
                            We’re a team of 4 geeks who are passionate about
                            art, blockchain and making unique stuff. What we do
                            is our way of life, this is how we live and always
                            bring our passion and creativity to the table, not
                            forgetting a little bit of fun.
                        </p>

                        <p className="mb-8">
                            We are coming from a grassroots startup and
                            VeeFriends inspired us to make this project. Because
                            of that we will bring the NFT to real world usage.
                            We are pushing to integrate NFTs and real-world
                            assets. The SSS NFTs are your access tokens.
                        </p>

                        <p className="mb-8">
                            <a href="https://twitter.com/prasdiman">
                                {" "}
                                <span className="text-blue-900 font-bold">
                                    The Initiator
                                </span>{" "}
                                - Prasdiman
                            </a>{" "}
                            is responsible for creative direction and project
                            management.
                        </p>

                        <p className="mb-8">
                            <a href="https://twitter.com/AriefWitjaksana">
                                {" "}
                                <span className="text-blue-900 font-bold">
                                    The Artist
                                </span>{" "}
                                - Arief Witjaksana
                            </a>{" "}
                            is responsible for the creation of all unique SSS
                            character illustrations.
                        </p>

                        <p className="mb-8">
                            <a href="https://twitter.com/the_bot999">
                                {" "}
                                <span className="text-blue-900 font-bold">
                                    The Technician
                                </span>{" "}
                                - The_Bot
                            </a>{" "}
                            is responsible for of all backend technical work
                            from smart contracts to servers.
                        </p>

                        <p className="mb-8">
                            <a href="https://twitter.com/goddamnft">
                                {" "}
                                <span className="text-blue-900 font-bold">
                                    The Speaker
                                </span>{" "}
                                - Goddamnft
                            </a>{" "}
                            is responsible for marketing and managing the
                            community.
                        </p>
                    </div>
                </div>
            </div>

            <div id="section-faq" className="bg-white">
                <div className="container mx-auto  py-24 px-8 lg:px-16 ">
                    <h2 className="text-4xl font-bold text-blue-900 mb-8">
                        FREQUENTLY ASKED QUESTIONS
                    </h2>
                    <SSSAccordion
                        items={[
                            {
                                title: "What’s an NFT?",
                                body: (
                                    <>
                                        <p>
                                            NFT stands for "Non-fungible token,"
                                            which means that it's a unique,
                                            digital item with blockchain-managed
                                            ownership that users can buy, own,
                                            and trade. Some NFT's fundamental
                                            function is to be digital art. But
                                            they can also offer additional
                                            benefits like exclusive access to
                                            websites, access to the place, and
                                            ownership records for physical
                                            objects. Think of it as a unique
                                            piece of art that can also work as a
                                            "members-only" card. SSS works like
                                            this.
                                        </p>
                                        <br></br>
                                    </>
                                ),
                                collapse: false,
                            },
                            {
                                title: "How to buy NFT?",
                                body: (
                                    <>
                                        <p className="mb-4">
                                            New to NFTs? No worries, here are
                                            some steps on what you need to do to
                                            get your SSS NFT.
                                        </p>
                                        <ul className="list-item">
                                            <li className="mb-4">
                                                1. Download the metamask.io
                                                extension for the Chrome/Brave
                                                browser or app on mobile. This
                                                will allow you to make purchases
                                                with Ethereum and can be found
                                                in the extensions tab. If you
                                                are on mobile, you must use the
                                                Metamask App Browser
                                            </li>
                                            <li className="mb-4">
                                                2. You can purchase Ethereum
                                                through the Metamask Wallet
                                                using Wyre or Send Ethereum from
                                                an exchange like Coinbase.
                                            </li>
                                            <li className="mb-4">
                                                3. Click on Connect at the top
                                                of the page and connect your
                                                Metamask. Once joined, you will
                                                be able to purchase the NFTs in
                                                the mint section. You will be
                                                prompted to sign your
                                                transaction. There will be a fee
                                                associated with every
                                                transaction related to gas
                                                prices.
                                            </li>
                                            <li className="mb-4">
                                                4. Once you have made your
                                                purchase, your SSS NFTs will be
                                                viewable in your wallet and on
                                                OpenSea.
                                            </li>
                                        </ul>
                                    </>
                                ),
                                collapse: false,
                            },
                            {
                                title: "What is real world utility?",
                                body: (
                                    <>
                                        <p>
                                            SSS offer you not just an amazing
                                            artwork but tobe use in the real
                                            world. SSS team will buy the
                                            property located in Bali to build an
                                            art gallery & hangout place
                                            especially for SSS holders. The
                                            holders of SSS will have rights to
                                            access the chill room on the gallery
                                            and voting or recommendation rights
                                            on the holders or the artists or the
                                            projects artworks to be displayed at
                                            Superlative Gallery located in Bali
                                            (up to 20 artworks/month tobe
                                            displayed on the gallery).
                                        </p>
                                        <br></br>
                                    </>
                                ),
                                collapse: false,
                            },
                            {
                                title: "Why Physical Art Gallery?",
                                body: (
                                    <>
                                        <p>
                                            Because of the sales commision. This
                                            is the main form of income for most
                                            conventional galleries. Commission
                                            is the percentage of the art sale
                                            price that a gallery keeps, with a
                                            reminder of showing appreciation to
                                            the artist (artist are
                                            rewarded/getting paid). It can vary
                                            wildly from gallery to gallery, with
                                            the average of 40–50%, although we
                                            have seen commission as low as 10%,
                                            and heard about commission as high
                                            as 70%. Commission is variable
                                            depending on the gallery’s expenses,
                                            what level of service the gallery
                                            offers, and the reputation of both
                                            the artist and the gallery.
                                        </p>
                                        <br></br>
                                    </>
                                ),
                                collapse: false,
                            },
                            {
                                title: "Why Bali?",
                                body: (
                                    <>
                                        <p>
                                            Beyond stunning beaches and magical
                                            temples, Bali has virtually every
                                            kind of natural beauty. Glorious
                                            mountainous areas with lush
                                            greenery, scenic lakes, gorgeous
                                            waterfalls, iconic rice fields,
                                            flower gardens, gushing sacred
                                            rivers and secret canyons all make
                                            up the island's landscape. So many
                                            different areas within Bali, some
                                            more touristy and crowded than
                                            others, but each holding unique
                                            appeal. Bali offers something for
                                            everyone whether that’s shopping,
                                            surfing, relaxing, retreating,
                                            serenity-seeking, sight-seeing,
                                            attractions, exploring, or indulging
                                            in the wide options for food and
                                            drink. Bali also has been recognized
                                            as one of the most popular
                                            destinations in the world, according
                                            to travel platform TripAdvisor
                                            Travelers’ Choice Awards of 2020 so
                                            because all of that the art gallery
                                            will fit located in bali.
                                        </p>
                                        <br></br>
                                    </>
                                ),
                                collapse: false,
                            },
                            {
                                title: "What benefit do I get from collect an SSS NFT?",
                                body: (
                                    <>
                                        <p>
                                            Many Things!!! This is a
                                            community-driven project. Being a
                                            member of Secret Superlative Society
                                            is much more than just having a NFT
                                            avatar with amazing unique artwork
                                            different than others in the nft
                                            space. We believed all SSS NFT is
                                            special, all SSS NFT holders will
                                            get the privilege of:
                                        </p>
                                        <br></br>
                                        <p>
                                            1. Commercial Rights over the
                                            artwork. <br></br>2. Exclusive
                                            access to chill room located in
                                            Bali. <br></br>3. A chance to be
                                            shipped a Signed framed print of
                                            your SSS NFT (up to 111 will be
                                            shipped). <br></br>4. Access for
                                            voting and recommendations on
                                            artists or projects that the
                                            Superlative Secret Society
                                            Foundation should support. <br></br>
                                            5. Access for voting and
                                            recommendations on the artists or
                                            the projects artworks tobe displayed
                                            at Superlative Gallery located in
                                            Bali (up to 20artworks/month will be
                                            displayed on the gallery). <br></br>
                                            6. Access to exclusive limited
                                            merchandise. <br></br>7. The chance
                                            of your artworks tobe displayed
                                            permanently at Superlative Gallery
                                            located in Bali.
                                        </p>
                                        <br></br>
                                    </>
                                ),
                                collapse: false,
                            },
                            {
                                title: "What is SSS Foundation?",
                                body: (
                                    <>
                                        <p>
                                            Basically SSS Foundation is the
                                            community wallet, we will estabilish
                                            a 50 ETH community wallet All SSS
                                            holders have voting rights to decide
                                            what to do with it, as a part of
                                            Superlative Secret Society
                                            Foundation.
                                        </p>
                                        <br></br>
                                    </>
                                ),
                                collapse: false,
                            },
                        ]}
                    />
                </div>
            </div>

            <div className="blue-bg-footer">
                <div className="container mx-auto flex flex-col py-16 items-center justify-center">
                    <div className="w-1/12 mb-8">
                        <img src="images/sss-logo-min.png" alt="" />
                    </div>
                    <div className="w-full mb-8">
                        <ul className="flex justify-center">
                            <li className="inline-block">
                                <a
                                    className="inline-block px-4 text-white"
                                    href="#section-home"
                                >
                                    Home
                                </a>
                            </li>
                            <li className="inline-block">
                                <a
                                    className="inline-block px-4 text-white"
                                    href="#section-about"
                                >
                                    About
                                </a>
                            </li>
                            <li className="inline-block">
                                <a
                                    className="inline-block px-4 text-white"
                                    href="#section-roadmap"
                                >
                                    Roadmap
                                </a>
                            </li>
                            <li className="inline-block">
                                <a
                                    className="inline-block px-4 text-white"
                                    href="#section-team"
                                >
                                    Team
                                </a>
                            </li>
                            <li className="inline-block">
                                <a
                                    className="inline-block px-4 text-white"
                                    href="#section-faq"
                                >
                                    FAQ
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div className="w-full mb-8">
                        <ul className="flex justify-center">
                            <li className="inline-block">
                                <a
                                    className="inline-block px-4 text-white text-opacity-20"
                                    href="https://twitter.com/SuperlativeSS"
                                >
                                    <AiOutlineTwitter size={36} />
                                </a>
                            </li>
                            <li className="inline-block">
                                <a
                                    className="inline-block px-4 text-white text-opacity-20"
                                    href="https://discord.gg/Akb4afTAZz"
                                >
                                    <SiDiscord size={36} />
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div className="w-full mb-8">
                        <p className="text-center text-white text-opacity-20">
                            Copyright© Superlative Secret Society 2021. All
                            rights reserved
                        </p>
                    </div>
                </div>
            </div>
        </>
    );
}
